<?

function fooo($arg_1, $arg_2, /* ..., */ $arg_n)
{
  echo "Exemple de fonction.\n";
  return $retval;
};

// Les noms de fonctions suivent les mêmes règles que les autres labels en PHP. Un nom de fonction valide commence par une lettre ou un souligné, suivi par un nombre quelconque de lettres, de nombres ou de soulignés. Ces règles peuvent être représentées par l'expression rationnelle suivante : ^[a-zA-Z_\x80-\xff][a-zA-Z0-9_\x80-\xff]*$


// Lorsqu'une fonction est définie de manière conditionnelle, comme dans les exemples ci-dessous, leur définition doit précéder leur utilisation. 

$makefoo = true;

/* Impossible d'appeler foo() ici,
   car cette fonction n'existe pas.
   Mais nous pouvons utiliser bar() */

bar();

if ($makefoo) {
  function foo()
  {
    echo "Je n'existe pas tant que le programme n'est pas passé ici.\n";
  }
}

/* Maintenant, nous pouvons appeler foo()
   car $makefoo est évalué à vrai */

if ($makefoo) foo();

function bar()
{
  echo "J'existe dès le début du programme.\n";
}
//
//