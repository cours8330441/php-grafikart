        <?php
         $ressource = fopen('test.txt', 'rb');
         echo fread($ressource, filesize('test.txt'));
         ?>

        /*

        Ici, on commence par utiliser fopen() pour lire notre fichier et on récupère la ressource renvoyée par la
        fonction dans une variable $ressource.

        On passe ensuite le contenu de notre variable à fread() et on lui demande de lire le fichier jusqu’au bout en
        lui passant la taille exacte du fichier obtenue avec filesize().

        Finalement, on affiche le résultat renvoyé par fread() grâce à une structure echo.
        Lire un fichier ligne par ligne avec fgets()

        La fonction fgets() va nous permettre de lire un fichier ligne par ligne. On va passer le résultat renvoyé par
        fopen() en argument de fgets() et à chaque nouvel appel de la fonction, une nouvelle ligne du fichier va pouvoir
        être lue.

        On peut également préciser de manière facultative un nombre en deuxième argument de fgets() qui représente un
        nombre d’octets. La fonction lira alors soit le nombre d’octets précisé, soit jusqu’à la fin du fichier, soit
        jusqu’à arriver à une nouvelle ligne (le premier des trois cas qui va se présenter). Si aucun nombre n’est
        précisé, fgets() lira jusqu’à la fin de la ligne.

        Notez que si on précise un nombre d’octets maximum à lire inférieur à la taille de notre ligne et qu’on appelle
        successivement fgets(), alors la fin de la ligne courante sera lue lors du deuxième appel de fgets().

        */